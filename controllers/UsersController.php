<?php

namespace app\controllers;

use app\models\ResetPassword;
use Yii;
use app\models\Users;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
/*     public function actionIndex()
    {
        $searchModel = new SearchUsers();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    } */

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if(!Yii::$app->user->isGuest){
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }else{
            throw new \yii\web\ForbiddenHttpException;
        } 
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
/*     public function actionCreate()
    {
        $model = new Users();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->user_id]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }
 */
    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        if(!Yii::$app->user->isGuest){

            $id = Yii::$app->user->identity->user_id;
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) ) {
                $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                $model->upload();
                $model->save();
                return $this->redirect(['view','id' => $model->user_id]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }else{
            throw new \yii\web\ForbiddenHttpException;
        } 
    }
    public function actionChangePassword()
    {
        if(!Yii::$app->user->isGuest){
        
            $alert = 0;
            $id = Yii::$app->user->identity->user_id;
            $model = $this->findModel($id);
            $password = $model->password;
            $model->scenario = Users::SCENARIO_PASSWORD_CHANGE;
            if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            
            if ($model->load(Yii::$app->request->post())) {
                if(!Yii::$app->getSecurity()->validatePassword($model->old_password, $password)){
                    $model->addError('old_password', 'Incorrect password');
                    $model->password = "";
                    $model->password_repeat = "";
                }else{
                    $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
                    $model->password_repeat = $model->password;
                    if($model->save()){
                        $alert = 1;
                        
                    }
                }
                //return $this->redirect(['view','id' => $model->user_id]);
            }
            $model->password = "";
            $model->password_repeat = "";
            $model->old_password = "";
            return $this->render('change-password', [
                'model' => $model,
                'alert' => $alert,
            ]);
        }else{
            throw new \yii\web\ForbiddenHttpException;
        } 
    }
    public function actionForgotPasswordReset()
    {
        $alert = 0;
        $model = new Users();
        if(Yii::$app->request->get('user') && Yii::$app->request->get('email')){
            $email = Yii::$app->request->get('email');
            $hash = Yii::$app->request->get('user');
            $check_date = date("Y-m-d H:i:s", strtotime('-15 minutes'));
            $result = ResetPassword::find()
                            ->where(['email' => $email])
                            ->andWhere(['reset_hash' => $hash])
                            ->andWhere(['>','created_at', $check_date])
                            ->one();
            if($result){
                $model = Users::find()
                            ->where(['email' => $email])
                            ->one();
            }else{
                return $this->render('error');

            }
        }
        $password = $model->password;
        $model->scenario = Users::SCENARIO_FORGOT_PASS;
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        
        if ($model->load(Yii::$app->request->post())) {
                $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
                $model->password_repeat = $model->password;
                if($model->save()){
                    ResetPassword::deleteAll(['email' => $model->email]);
                    $alert = 1;
                }
            //return $this->redirect(['view','id' => $model->user_id]);
        }
        $model->password = "";
        $model->password_repeat = "";
        return $this->render('change-password-rest', [
            'model' => $model,
            'alert' => $alert,
        ]);
    }

    public function actionForgotPassword()
    {
        $email = "";
        $alert = 0;
        if (Yii::$app->request->post("email")) {
           $email = Yii::$app->request->post("email");
           $user = Users::find()
                            ->where(['email' => $email])
                            ->one();
            $check_date = date("Y-m-d H:i:s", strtotime('-15 minutes'));
            $links = ResetPassword::find()
                                ->where(['email' => $email])
                                ->andWhere(['>','created_at', $check_date])
                                ->all();
            if(!$user){
                $alert = 1;
            }else if($links){
                $alert = 3;
            }else{
                
                $string = microtime() . $user->user_id;
                $hash = Yii::$app->getSecurity()->generatePasswordHash($string);
                $url = "https://invoicetelly.in/web/index.php?r=users/forgot-password-reset&user=$hash&email=$email";
                $result = Yii::$app->mailer->compose('reset_template', ['url' => $url])
                                ->setFrom('invoicetelly@gmail.com')
                                ->setTo($email)
                                ->setSubject('Reset password to Invoice Telly account')
                                ->send();
                if($result){
                    $alert = 2;
                    $reset_password = new ResetPassword();
                    $reset_password->email = $email;
                    $reset_password->reset_hash = $hash;
                    $reset_password->save();
                }else{
                    $alert = 4;
                }
                //send email
            }
           
        }
     
        return $this->render('forgot-password',[
            'alert' => $alert,
        ]);
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if(!Yii::$app->user->isGuest){

            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }else{
            throw new \yii\web\ForbiddenHttpException;
        } 
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
