<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Users;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
   /*  public function actionDashboard()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['invoice/index']);
        }else{
            return $this->redirect(['site/login']);
        }
    } */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['invoice/index']);
        }else{
            return $this->redirect(['site/login']);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $newuser = new Users();
        $newuser->scenario = Users::SCENARIO_REGISTER;
        if(Yii::$app->request->isAjax && $newuser->load(Yii::$app->request->post())){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($newuser);
        }

        if ($newuser->load(Yii::$app->request->post())) {
            $decrypt_pass = $newuser->password;
            $newuser->password = Yii::$app->getSecurity()->generatePasswordHash($newuser->password);
            $newuser->password_repeat = $newuser->password;
            if($newuser->save()){
                $model->username = $newuser->email;
                $model->password = $decrypt_pass;
                $model->login();
                return $this->redirect(['users/update']);
            }
            //return $this->goBack();
        }
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['invoice/index']);
        }
        
        return $this->render('login', [
            'model' => $model,
            'newuser' => $newuser,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(['login']);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
}
