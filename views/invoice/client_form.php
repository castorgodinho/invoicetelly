<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-form">
<?php

    $this->registerJs(
    '$("document").ready(function(){ 
            $("#new_country").on("pjax:end", function() {
                $.pjax.reload({container:"#countries"});  //Reload GridView
                $("#modal-id").modal("hide");
            });
        });'
    );
?>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
        <?php yii\widgets\Pjax::begin(['id' => 'new_country']) ?>
            <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true ]]); ?>

                <?= $form->field($client, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($client, 'address1')->textInput(['maxlength' => true]) ?>

                <?= $form->field($client, 'address2')->textInput(['maxlength' => true]) ?>

                <?= $form->field($client, 'phone_number')->textInput(['maxlength' => true]) ?>

                <?= $form->field($client, 'email')->textInput(['maxlength' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('<span class=\'glyphicon glyphicon-ok\'> </span> Save', ['class' => 'btn btn-default']) ?>
                </div>

            <?php ActiveForm::end(); ?>
            <?php yii\widgets\Pjax::end() ?>
        </div>
        <div class="col-md-1"></div>
    </div>

</div>


