<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = "Business Profile";
?>
<div class="users-view">
   <div class="text-right">
        <p>
            <?= Html::a('<span class=\'glyphicon glyphicon-pencil\'></span> Update', ['update', 'id' => $model->user_id], ['class' => 'btn btn-default']) ?>
        </p>
   </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'business_name',
            'address1',
            'address2',
            'telephone',
            'mobile_no',
            'email:email',
            'pan',
            'gstin',
            'bank_name',
            'account_number',
            'ifsc_code',
            'gpay',
            'phonepe',
            [
                'label' => "Company Logo",
                'format' => 'html',
                'value' => function($model){
                    if($model->logo != ""){
                        $url = $model->logo;
                        return Html::img($url, ['height' => '100']);
                    } 
                },
            ],
        ],
    ]) ?>

</div>
