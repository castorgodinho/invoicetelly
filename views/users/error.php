<?php 
    $this->title ="Invoice Telly - Error";
?>
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 text-center">
            <div class="jumbotron">
                <div class="container text-center">
                    <h1>Broken Link..</h1>
                    <p>The link seems to be broken or expired.</p>
                    <img style="margin: 0px auto;" src="img/logo.png" alt="" class="img-responsive img">
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
