<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .panel-default > .panel-heading {
        color: #333;
        background-color: #ececec;
        border-color: #f90000;
    }
</style>
<div class="users-form">

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="text-center">
                <h2>Your Business Profile</h2>
                <div class="line"></div>
            </div>
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            
            
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">Basic Information</div>
                    <div class="panel-body">
                        <?= $form->field($model, 'business_name')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'pan')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'gstin')->textInput(['maxlength' => true]) ?>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'imageFile')->fileInput()->label("Logo") ?>
                            </div>
                            <div class="col-md-6 text-center">
                                <?php 
                                    if($model->logo != ""){
                                        echo "<img src='$model->logo' class='img img-responsive' height='100' />";
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">Contact Information</div>
                    <div class="panel-body">
                        <?= $form->field($model, 'address1')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'address2')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'mobile_no')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                    </div>
            </div>
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">Banking Information</div>
                    <div class="panel-body">
                        <?= $form->field($model, 'bank_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'account_number')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'ifsc_code')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'gpay')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'phonepe')->textInput(['maxlength' => true]) ?>
                    </div>
            </div>
            


           


            

            <div class="form-group">
                <?= Html::submitButton('<span class=\'glyphicon glyphicon-ok\'> </span> Save', ['class' => 'btn btn-default']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-3"></div>
    </div>

</div>
