<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="icon" href="img/fav.png" type="image/gif" sizes="16x16">
    <link rel="manifest" href="manifest.json">

    <?php $this->head() ?>
    <style>
        .navbar-default {
            border: none;
        }
        .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus {
            /* text-decoration: underline; */
            /* color: #555; */
            border-bottom: 2px solid red;
            background-color: #5550;
            font-weight: bold;
        }
        .line{
            border: 2px solid red;
            background-color: red;
            width: 50px;
            margin: 15px auto;

        }
    </style>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php 
        $menu = [];
        $user = [];
        $user[] = ["label" => "Business Profile","url" => ['users/update'], "icon" => "fab fa-free-code-camp",'linkOptions' => ['data-method' => 'post']];
        $user[] = ["label" => "Reset Password","url" => ['users/change-password'], "icon" => "fab fa-free-code-camp",'linkOptions' => ['data-method' => 'post']];
        $user[] = ["label" => "Logout","url" => ['site/logout'], "icon" => "fab fa-free-code-camp",'linkOptions' => ['data-method' => 'post']];
        if(Yii::$app->user->isGuest){
            $menu[] = ["label" => "Login","url" => ['site/login'], "icon" => "fab fa-free-code-camp"];
        }else{
            //$menu[] = ["label" => "Dashboard","url" => ['site/dashboard'], "icon" => "fab fa-free-code-camp"];
            $menu[] = ["label" => "Invoice","url" => ['invoice/index'], "icon" => "fab fa-free-code-camp"];
            $menu[] = ["label" => "Clients","url" => ['client/index'], "icon" => "fab fa-free-code-camp",'linkOptions' => ['data-method' => 'post']];
            $menu[] = ["label" => "Report","url" => ['invoice/report'], "icon" => "fab fa-free-code-camp",'linkOptions' => ['data-method' => 'post']];
            $menu[] = [ "label" => "Account", "url" => "#", "items" => $user ];

        }
        

    ?>
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/img/logo.png', ['height'=>'20']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => ' navbar-default navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menu
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Invoice Telly <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>
<?php 
    $script = <<< JS
        $(document).ready(function(){
           
        if ('serviceWorker' in navigator) 
        { // 
            navigator.serviceWorker.register('service-worker.js',{
                scope: '.'
            }).then(function(reg) {
                console.log('Successfully registered service worker', reg);
            }).catch(function(err) {
                console.warn('Error whilst registering service worker', err);
            });
        }
    });
JS;
$this->registerJS($script);
?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
