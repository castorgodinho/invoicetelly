<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-form">

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            
            <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'address1')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'address2')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('<span class=\'glyphicon glyphicon-ok\'> </span> Save', ['class' => 'btn btn-default']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-3"></div>
    </div>

</div>
