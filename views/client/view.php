<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'address1',
            'address2',
            'phone_number',
            'email:email',
        ],
    ]) ?>

<?php 
    $query = \app\models\Invoice::find()->where(['client_id' => $model->client_id]);

    // add conditions that should always apply here

    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'sort' => [
            'defaultOrder' => [
                'invoice_id' => SORT_DESC,
            ]
        ],
    ]);
?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            /* 'invoice_id', */
            'invoice_number',
            /* [
                'label' => "Invoice Date",
                'value' => function($model){
                    return date('d M y', strtotime($model->invoice_date))
                }
            ], */
            'invoice_date',
            [
                'label' => "Client Name",
                'attribute' => 'client_name',
                'value' => 'client.name'
            ],
            [
                'label' => "Payment status",
                'value' => function($model){
                    if($model->payment_status == 1){
                        return "Paid";
                    }else{
                        return "Not Paid";
                    }
                }
            ],
            //'content:ntext',
            //'invoice_option:ntext',
            //'payment_status',
            //'client_id',
            //'created_at',
            //'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
                  'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='index.php?r=invoice/view&id='.$model->invoice_id;
                        return $url;
                    }
        
                    if ($action === 'update') {
                        $url ='index.php?r=invoice/update&id='.$model->invoice_id;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url ='index.php?r=invoice/delete&id='.$model->invoice_id;
                        return $url;
                    }
        
                  }
        
            ],
        ],
    ]); ?>
</div>

