<?php 
    use app\models\Invoice;
    use app\models\Client;
    $this->title = "Dashboard";
?>

<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&family=Titillium+Web:wght@300&display=swap" rel="stylesheet">
<style>
    body{
        font-family: 'Titillium Web', sans-serif;
        background-image: url('img/furley_bg.png');
    }
    .stat{
        background-color: #f9f9f9;
        text-align: center;
        margin: 20px;
        padding: 10px;
        box-shadow: 0px 1px 4px black;
        border-radius: 3px;
    }
    .stat-btn{
        text-align: center;
        margin: 20px;
        padding: 10px;
    }
    .dash-icon{
        color: #ad5f41;
    }
    .btn-default{
        border: 2px solid #ff000066;
    }
</style>
<?php 
    $user_id = Yii::$app->user->identity->user_id;
    $invoices_count = Invoice::find()->where(['user_id' => $user_id ])->count();
    $invoices_np_count = Invoice::find()->where(['payment_status' => 0])->andWhere(['user_id' => $user_id ])->count();
    $client_count = Client::find()->where(['is_deleted' => 0])->andWhere(['user_id' => $user_id ])->count();
?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="stat">
                <p><b>INVOICE COUNT</b></p>
                <h1><span class="dash-icon glyphicon glyphicon-stats"></span> <?= $invoices_count ?></h1>
            </div>
        </div>
        <div class="col-md-3">
            <div class="stat">
                <p><b>TOTAL CLIENTS</b></p>
                <h1><span class="dash-icon glyphicon glyphicon-user"></span> <?= $client_count ?></h1>
            </div>
        </div>
        <div class="col-md-3">
            <div class="stat">
                <p><b>TOTAL PENDING INVOICES</b></p>
                <h1><span class="dash-icon glyphicon glyphicon-list"></span> <?= $invoices_np_count ?></h1>
            </div>
        </div>
        <div class="col-md-3">
            <div class="stat-btn">
                <a class="btn btn-default btn-block" href="index.php?r=invoice%2Fcreate"><span class="glyphicon glyphicon-plus"> </span> New Invoice</a>
                <br><a class="btn btn-default btn-block" href="index.php?r=client%2Fcreate"><span class="glyphicon glyphicon-plus"> </span> Add Client</a>
            </div>
        </div>
    </div>
</div>