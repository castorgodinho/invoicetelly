<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    body{
        background-image: url('img/office-background.jpg');
        background-repeat: no-repeat;
        background-size: cover;
    }
    .login-div {
        padding: 20px;
        border-radius: 4px;
        color: #cecece;
        background-color: #000000a8;
    }

    .form-control {
        background-color: #00000000;
        border: 1px solid #cecece;
        color: #cecece;
    }
    .btn-default {
        color: #cecece;
        background-color: #fff0;
        border-color: #cecece;
    }
    .modal-content {
        background-color: #000000b3;
        color: #d0d0d0;
    }
    .modal-header {
        padding: 15px;
        background-color: #ff0000a6;
        border-bottom: 1px solid #ff0000a6;
    }
    .modal-footer {
        padding: 15px;
        text-align: right;
        border-top: none;
    }
    .btn-default:hover {
        color: #fff;
        background-color: #f3000000;
        border-color: #ff0000;
    }
    .btn-default:focus, .btn-default.focus {
        color: #e2e2e2;
        background-color: #e6e6e600;
        border-color: #ff0000;
    }
    
    .btn-default:active, .btn-default.active, .open > .dropdown-toggle.btn-default {
        color: #fff;
        background-color: #e6e6e6;
        border-color: #adadad;
    }
</style>
<div class="site-login">
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class="login-div">
                <h1><?= Html::encode($this->title) ?></h1>

                <p>Please fill out the following fields to login:</p>

                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    //'layout' => 'horizontal',
                    /* 'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-6\">{input}{error}</div>\n",
                        'labelOptions' => ['class' => 'col-lg-2 control-label'],
                    ], */
                ]); ?>

                    <?= $form->field($model, 'username')->textInput() ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>

                    <div class="form-group">
                        <div class="c" style="margin-bottom: 8px;">
                            <?= Html::submitButton('<span class=\'glyphicon glyphicon-ok\'> </span> Login', ['class' => 'btn btn-default', 'name' => 'login-button']) ?>
                            <a href="#" class="register btn btn-default" data-toggle="modal" data-target="#exampleModal"><span class="glyphicon glyphicon-user"></span> Create account</a>
                        </div>
                        <a href="index.php?r=users/forgot-password">Forgot password</a>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="col-md-6">
            
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
     <?php $form2 = ActiveForm::begin(['id' => 'registration-form','enableAjaxValidation' => true,]); ?>
    <div class="modal-content">
      <div class="modal-header text-center" style="display: inline-block; width: 100%;">
        <h3 class="modal-title" id="exampleModalLabel"><span class="glyphicon glyphicon-user"></span> NEW ACCOUNT</h3>
      </div>
      <div class="modal-body">
            <?= $form2->field($newuser, 'business_name')->textInput(['maxlength' => true]) ?>

            <?= $form2->field($newuser, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form2->field($newuser, 'password')->passwordInput() ?>
            <?= $form2->field($newuser, 'password_repeat')->passwordInput() ?>
      </div>
      <div class="modal-footer">
        
        <div class="form-group">
            <?= Html::submitButton('<span class=\'glyphicon glyphicon-plus\'> </span> Create Account', ['class' => 'btn btn-default']) ?>
        </div>

      </div>
    </div>
    <?php ActiveForm::end(); ?>
  </div>
</div>

<?php 
    $script = <<< JS
        $(document).ready(function(){
            $(".register").click(function(){
                $(".login-div").fadeOut("fast");
            });
            $('#exampleModal').on('hidden.bs.modal', function (e) {
                console.log("Hidden");
                $(".login-div").fadeIn("fast");

            })
        });
JS;
    $this->registerJS($script);
?>