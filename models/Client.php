<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client".
 *
 * @property int $client_id
 * @property string $name
 * @property string $address1
 * @property string $address2
 * @property string $phone_number
 * @property string $email
 * @property int $is_deleted
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property Users $user
 * @property Invoice[] $invoices
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['is_deleted', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'address1', 'address2', 'email'], 'string', 'max' => 100],
            [['phone_number'], 'string', 'max' => 15],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_id' => 'Client ID',
            'name' => 'Name',
            'address1' => 'Address Line 1',
            'address2' => 'Address Line 2',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['client_id' => 'client_id']);
    }
}
