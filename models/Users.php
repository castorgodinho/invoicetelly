<?php

namespace app\models;

use ErrorException;
use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $user_id
 * @property string $business_name
 * @property string $address1
 * @property string $address2
 * @property string $telephone
 * @property string $mobile_no
 * @property string $email
 * @property string $pan
 * @property string $logo
 * @property string $password
 * @property string $bank_name
 * @property string $account_number
 * @property string $ifsc_code
 * @property int $is_deleted
 * @property string $created_at
 * @property string $updated_at
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public $password_repeat;
    public $old_password;
    const SCENARIO_REGISTER = 'register';
    const SCENARIO_PASSWORD_CHANGE = 'change password';
    const SCENARIO_FORGOT_PASS = 'forgot password';
    public $imageFile;
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'email'], 'required'],
            [['imageFile'], 'file', 'skipOnEmpty' => true],
            [['logo', 'business_name'], 'string'],
            [['email'], 'email'],
            [['is_deleted'], 'integer'],
            [
                'email', 'unique',
                'targetClass' => Users::className(),        
                'message' => 'This email address has already been taken.'
            ],
            [['created_at', 'updated_at'], 'safe'],
            [['password'], 'required', 'on' => self::SCENARIO_REGISTER],
            [['password_repeat', 'password'], 'required', 'on' => self::SCENARIO_REGISTER],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match", 'on' => self::SCENARIO_REGISTER ],
            [['password'], 'required', 'on' => self::SCENARIO_PASSWORD_CHANGE],
            [['password_repeat', 'password', 'old_password'], 'required', 'on' => self::SCENARIO_PASSWORD_CHANGE],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match", 'on' => self::SCENARIO_PASSWORD_CHANGE ],
            [['business_name', 'address1', 'address2', 'email', 'bank_name'], 'string', 'max' => 100],
            [['telephone', 'mobile_no', 'ifsc_code'], 'string', 'max' => 20],
            [['pan', 'account_number', 'gstin', 'gpay','phonepe'], 'string', 'max' => 50],
            [['password_repeat', 'password'], 'required', 'on' => self::SCENARIO_FORGOT_PASS],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match", 'on' => self::SCENARIO_FORGOT_PASS ],
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'business_name' => 'Business Name',
            'address1' => 'Address Line 1',
            'address2' => 'Address Line 2',
            'telephone' => 'Telephone Number',
            'mobile_no' => 'Mobile Number',
            'gstin' => 'GSTIN',
            'gpay' => 'Google Pay',
            'phonepe' => 'PhonePe',
            'pan' => 'PAN',
            'logo' => 'Logo',
            'password' => 'Password',
            'bank_name' => 'Bank Name',
            'account_number' => 'Account Number',
            'ifsc_code' => 'IFSC Code',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            if($this->imageFile){
                try{
                    unlink($this->logo);
                }catch(ErrorException $e){

                }
                $this->imageFile->saveAs('uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
                $this->logo = 'uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
            }
            return true;
        } else {
            return false;
        }
    }
    
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    public function getId()
    {
        return $this->user_id;
    }

    public function getAuthKey()
    {
        return null;
    }

    public function validateAuthKey($authKey)
    {
        return null;
    }

    public static function findByUsername($username)
    {
        return Users::find()
            ->where(['email' => $username])->one();
        //return null;
    }
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    
}
